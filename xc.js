const fs=require('fs');
const phantom=require('phantom');
let firsturl='http://hotels.ctrip.com/hotel/dali36';
let jqueryurl='http://47.92.67.128/jquery.js';
let ajaxurl='http://hotels.ctrip.com/Domestic/Tool/AjaxHotelList.aspx';
let file='xc.csv';
let instance,page;
let datalist=[];
let ajaxstate=[1];
let waittime=20;
let pagenum=1;

//请求数据
async function request(){
	writelog([{name:'name',address:'address',area:'area',price:'price',level:'level',cent:'cent',special:'special',recommend:'recommend',service:'service'}]);
	instance=await phantom.create(['--ignore-ssl-errors=yes','--load-images=no']);
	page=await instance.createPage();
	page.setting('userAgent','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Maxthon/5.1.2.3000 Chrome/55.0.2883.75 Safari/537.36');
	await page.on('onResourceRequested',function(requestData,networkRequest){
		//console.info('Requesting',requestData.url);
		if(requestData.url==ajaxurl){
			pagenum=pagenum+1;
			console.log('正在抓取第'+pagenum+'页数据！');
		}
	});
	await page.on('onResourceReceived',function(response){
		if(response.url==ajaxurl){
			ajaxstate[0]=1;
		}
	});
	await page.on('onConsoleMessage',function(msg,lineNum,sourceId){
		console.log(msg);
	});
	await page.on('onError',function(msg,trace){
		console.log(msg);
	});
	console.log('正在抓取第'+pagenum+'页数据！');
	let status=await page.open(firsturl);
	await page.includeJs(jqueryurl);
	if(status=='success'){
		getData();				
	}
	else{
		console.log('无法访问网页！');
	}
}
//解析数据   state 1正常 2无数据 3最后一个
async function getData(){
	let content=await page.evaluate(function(ajaxstate){
		var state=1;
		var data=[];
		if(ajaxstate[0]==0){
			state=2;
			return {state:state,data:null};
		}
		if($$("#downHerf").attr('class')=='c_down_nocurrent'){
			state=3;
		}
		$$("#hotel_list").find("div.hotel_new_list").each(function(){
			var name=$$(this).find('h2.hotel_name').find('a').attr('title');
			var address=$$(this).find('p.hotel_item_htladdress').text();
			var area=$$(this).find('p.hotel_item_htladdress').find('a').first().text();
			var price=$$(this).find('span.J_price_lowList').text();
			var level=$$(this).find('span.hotel_level').text();
			var cent=$$(this).find('span.hotel_value').text();
			var recommend=$$(this).find('span.recommend').text();
			var special='';
			var service='';
			var speciallength=$$(this).find('span.special_label').find('i').length;
			var servicelength=$$(this).find("div.icon_list").find('i').length;
			$$(this).find("span.special_label").find('i').each(function(index){
				var specialitem=$$(this).text();
				if(index==speciallength-1){
					special+=specialitem;
				}
				else{
					special+=specialitem+' ';
				}
			});
			$$(this).find("div.icon_list").find('i').each(function(index){
				var serviceitem=$$(this).attr('title');
				if(index==servicelength-1){
					service+=serviceitem;
				}
				else{
					service+=serviceitem+' ';
				}
			});
			var item={name:name,address:address,area:area,price:price,level:level,cent:cent,special:special,recommend:recommend,service:service};
			data.push(item);
		});
		ajaxstate[0]=0;
		document.getElementById("downHerf").click();
		return {state:state,data:data};
	},ajaxstate);
	if(content.state==3){
		datalist.push(content.data);
		console.log(content.data);
		writelog(content.data);
		await instance.exit();
		return;
	}
	else if(content.state==2){
		setTimeout(function(){
			console.log('do');
			getData();
		},waittime*1000);
	}
	else{
		datalist.push(content.data);
		console.log(content.data);
		writelog(content.data);
		setTimeout(function(){
			console.log('do');
			getData();
		},waittime*1000);
	}
}
//写入日志
function writelog(data){
	let str='';
	for(let i=0;i<data.length;i++){
		str+=data[i].name+','+data[i].address+','+data[i].area+','+data[i].price+','+data[i].level+','+data[i].cent+','+data[i].special+','+data[i].recommend+','+data[i].service+"\r\n";
	}
	let ws=fs.createWriteStream(file,{flags:'a'});
	ws.end(str,(err)=>{
		if(err){
			console.log(err);
		}
	});
}
//主函数
function main(){
	console.log('正在抓取数据，抓取完成后会自动退出程序！');
	request();
}
main();